<?php
$args = array(
    'post_type'   => 'tribe_events',
    'posts_per_page'   => 6,
);

$tribe_events = new WP_Query( $args ); ?>

<div class="section-events section-bg-image-color ptb-80" style="background-image: url('<?php echo get_template_directory_uri()?>/dist/images/home/istockphoto-869288556-2048x2048.jpg'); background-color: #eb6321;">
    <div class="container">
        <?php if( $tribe_events->have_posts() ) :
            ?>
            <div class="content">
                <h2>Upcoming Events</h2>
                <div class="row">
                    <?php
                    while( $tribe_events->have_posts() ) :
                        $tribe_events->the_post();
                        ?>
                        <div class="col-md-6 col-lg-4">
                            <div class="post-event">
                                <div class="post-event-l">
                                    <span class="meta-month"><?php echo tribe_get_start_date($post,false,'M'); ?></span>
                                    <span class="meta-date"><?php echo tribe_get_start_date($post,false,'d'); ?></span>
                                </div>
                                <div class="post-event-r">
                                    <a href="<?php the_permalink()?>"><h3><?php the_title(); ?></h3></a>
                                    <span class="meta-location"><i class="icon-pin"></i> <?php echo tribe_get_city(); ?>, <?php echo tribe_get_country(); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php
        else :
            esc_html_e( 'No testimonials in the diving taxonomy!', 'text-domain' );
        endif;
        ?>
    </div>
</div>




