<?php $section_updates = get_sub_field('section_updates'); ?>
<div class="section-updates ptb-80" style="background-color: <?php echo $section_updates['background_color']; ?>">
    <div class="container">
        <div class="content">
            <?php if ($section_updates = $section_updates['item']):?>
                <div class="row">
                    <?php foreach ($section_updates as $item): ?>
                        <div class="col-md-6 col-lg-4">
                            <?php echo (!empty($item['link']['url'])) ? ' <a href="'.$item['link']['url'].'">':''; ?>
                            <div class="content-block" style="background-image: url('<?php echo $item['background_image']['url']; ?>')">
                                <div class="content-block-item">
                                    <div class="content-text">
                                        <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>">
                                        <?php echo $item['content']; ?>
                                    </div>
                                </div>
                            </div>
                            <?php echo (!empty($item['link']['url'])) ? '</a>':''; ?>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>