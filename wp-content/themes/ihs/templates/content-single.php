<?php /*get_template_part('templates/content-single', get_post_type()); */?>
<?php get_template_part('templates/page', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>
    <div class="section-single-post section-archive-hormones-single ptb-80">
        <div class="container">
            <div class="content-with-sidebar content-with-sidebar-left">
                <div class="content-main">
                    <div class="post-list-inline">
                        <div class="post-list-inline-details">
                            <h1><?php the_title(); ?></h1>
                            <?php the_content()?>
                        </div>
                    </div>
                </div>
                <?php get_template_part('templates/sidebar', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
            </div>
        </div>
    </div>
<?php endwhile; ?>
