<form role="search" method="get" id="search-form" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'presentation' ); ?></label>
        <input type="search" placeholder="<?php echo esc_attr( 'Search…', 'presentation' ); ?>" class="form-control" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
        <div class="input-group-prepend">
            <button class="btn input-group-text btn-warning" type="submit" id="search-submit">Search</button>
        </div>
    </div>
</form>