<div class="top-header">
    <div class="container">
        <div class="top-header-content">
            <?php $top_navigation_links = get_field('top_navigation_links','option');?>
            <?php if ($top_navigation_links): ?>
            <ul class="list-inline">
                <?php foreach ($top_navigation_links as $item):?>
                    <?php if ($item['icon'] == "separator"): ?>
                        <li class="list-inline-item">
                            <?php echo !empty($item['link']['url']) ? '<a href="'.$item['link']['url'].'">':''; ?>
                                <?php echo "<span>|</span>"?>
                                <?php echo !empty($item['link']['title']) ? $item['link']['title']:''; ?>
                            <?php echo !empty($item['link']['url']) ? '</a>':''; ?>
                        </li>
                    <?php elseif($item['icon'] == "icon-magnifying-glass"): ?>
                        <li class="list-inline-item">
                            <form role="search" method="get" id="search-form" class="seearch-form-expand" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <input type="search" placeholder="<?php echo esc_attr( 'Search…', 'presentation' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
                                <?php echo !empty($item['link']['url']) ? '<a href="'.$item['link']['url'].'">':''; ?>
                                <?php echo "<i class='".$item['icon']."'></i>"?>
                                <?php echo !empty($item['link']['title']) ? $item['link']['title']:''; ?>
                                <?php echo !empty($item['link']['url']) ? '</a>':''; ?>
                            </form>
                        </li>
                    <?php else: ?>
                        <li class="list-inline-item">
                            <?php echo !empty($item['link']['url']) ? '<a href="'.$item['link']['url'].'">':''; ?>
                                <?php echo "<i class='".$item['icon']."'></i>"?>
                                <?php echo !empty($item['link']['title']) ? $item['link']['title']:''; ?>
                            <?php echo !empty($item['link']['url']) ? '</a>':''; ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach;?>
            </ul>
        <?php endif; ?>
        </div>
    </div>
</div>
<header class="navbar-header<?php echo (!is_front_page()) ? ' p-0':''; ?>">
    <div class="container">
        <nav class="navbar navbar-light nav-primary navbar-expand-xl">
            <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                <?php
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                ?>
                <img class="img-fluid" src="<?php echo $image[0]; ?>" alt="">
            </a>
            <button class="navbar-toggler hamburger hamburger--slider" type="button" data-toggle="collapse" data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
                <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>

            <div class="collapse navbar-collapse" id="navbar-primary">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu([
                        'theme_location' => 'primary_navigation',
                        'container' => false,
                        'depth' => 2,
                        'menu_class' => 'navbar-nav navbar-main-menu ml-auto',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker()
                    ]);
                endif;
                ?>
            </div>
        </nav>
    </div>
</header>
