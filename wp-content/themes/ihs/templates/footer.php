<?php $section_newsletter = get_field('newsletter','option')?>

<div class="section-newsletter section-bg-image-color ptb-80" style="background-image: url('<?php echo $section_newsletter['background_image']['url']; ?>'); background-color:  <?php echo $section_newsletter['background_color']; ?>;">
    <div class="container">
        <div class="content">
            <?php echo $section_newsletter['content']; ?>
        </div>
    </div>
</div>
<?php $section_footer  = get_field('footer','option')?>
<footer class="section-footer content-info ptb-80" style="background-color: <?php $section_footer['background_color']?>">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="content-block">
                        <a class="footer-logo" href="<?php echo $section_footer['footer_logo_link']['url']?>">
                            <img src="<?php echo $section_footer['footer_logo']['url']?>" alt="<?php echo $section_footer['footer_logo']['alt']?>">
                        </a>
                        <?php echo $section_footer['footer_content']?>
                    </div>
                </div>

                <?php if ($footer_links = $section_footer['footer_links']):?>
                    <?php foreach ($footer_links as $footer_link):?>
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <div class="content-block">
                                <h2><?php echo $footer_link['title']?></h2>
                                <?php if ($lists = $footer_link['list']):?>
                                    <ul>
                                        <?php foreach ($lists as $list):?>
                                            <li>
                                                <?php echo ($list['link']['url'] == "#") ? '':'<a href="'.$list['link']['url'].'">'?>
                                                <?php echo $list['link']['title']?>
                                                <?php echo ($list['link']['url'] == "#") ? '':'</a>';?>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <?php echo $section_footer['copyright']?>
        </div>
    </div>
</footer>
