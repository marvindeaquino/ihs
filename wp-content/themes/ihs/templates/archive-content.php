<?php
$current_post_type = get_post_type();
$archive = get_field('archive','option');
$archive_types = $archive['type'];
?>
<?php if ($archive_types):?>
    <?php foreach ($archive_types as $archive_type):?>
        <?php $content = $archive_type['content'];?>
        <?php if ($archive_type['post_type_slug'] == $current_post_type && !empty($content)): ?>
            <div class="archive-hormones-content ptb-80">
                <div class="container">
                    <div class="content-main">
                        <?php echo gc_remove_p_tags_around_images($content);?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach;?>
<?php endif;?>