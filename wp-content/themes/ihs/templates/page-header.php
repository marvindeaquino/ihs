<?php use Roots\Sage\Titles; ?>
<?php
$current_post_type = get_post_type();
$archive = get_field('archive','option');
$archive_types = $archive['type'];
?>
<?php if (is_post_type_archive() || is_tax() || (is_singular() && !is_page()) || tribe_is_event() || is_home()): ?>
    <?php if ($archive_types):?>
        <?php foreach ($archive_types as $archive_type):?>
            <?php if ($archive_type['post_type_slug'] == $current_post_type): ?>
                <?php if (is_post_type_archive($archive_type['post_type_slug'])): ?>
                    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php echo $archive_type['banner_image']['url'];?>');">
                        <div class="container">
                            <h1><?php echo !empty($archive_type['title']) ? $archive_type['title'] : post_type_archive_title();?></h1>
                        </div>
                    </div>
                <?php elseif (is_singular($archive_type['post_type_slug'])):?>
                    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php echo $archive_type['banner_image']['url'];?>');">
                        <div class="container">
                            <h1><?php echo !empty($archive_type['title']) ? $archive_type['title'] : Titles\title(); ?></h1>
                        </div>
                    </div>
                <?php elseif (is_tax()): ?>
                    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php the_post_thumbnail_url('Full')?>');">
                        <div class="container">
                            <h1><?php single_term_title(); ?></h1>
                        </div>
                    </div>
                <?php elseif (is_home()):?>
                    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php echo $archive_type['banner_image']['url'];?>');">
                        <div class="container">
                            <h1><?php echo !empty($archive_type['title']) ? $archive_type['title'] : Titles\title(); ?></h1>
                        </div>
                    </div>
                <?php else: ?>
                <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php echo $archive_type['banner_image']['url'];?>');">
                    <div class="container">
                        <h1><?php echo get_the_title(); ?></h1>
                    </div>
                </div>
                <?php endif;?>
            <?php endif; ?>
        <?php endforeach;?>
    <?php endif;?>
    <?php elseif (is_page()): ?>
    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php the_post_thumbnail_url('Full')?>');">
        <div class="container">
            <h1><?= Titles\title(); ?></h1>
        </div>
    </div>
    <?php elseif(is_search()):?>
    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php echo get_template_directory_uri()?>/dist/images/about/about-us-banner.jpg');">
        <div class="container">
            <h1><?php echo get_search_query();?></h1>
        </div>
    </div>
    <?php else: ?>

    <div class="section-subpage-banner section-bg-image-color" style="background-image: url('<?php the_post_thumbnail_url('Full')?>');">
        <div class="container">
            <h1><?= Titles\title(); ?></h1>
        </div>
    </div>
<?php endif;?>