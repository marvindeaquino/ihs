<?php /*get_template_part('templates/content-single', get_post_type()); */?>
<?php get_template_part('templates/page', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>
    <div class="section-single-post ptb-80">
        <div class="container">
            <div class="content-with-sidebar">
                <?php get_template_part('templates/sidebar', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <div class="content-main">
                    <div class="post-list-inline">
                        <div class="post-list-inline-img">
                            <?php the_post_thumbnail('books-single-thumbnail')?>
                        </div>
                        <div class="post-list-inline-details">
                            <h2><?php the_title(); ?></h2>
                            <?php $terms = get_terms('book_author'); ?>
                            <?php foreach ($terms as $term) : ?>
                                <?php $author = $term->name ?>
                                <?php $author = str_replace("'s Books","", str_replace('Dr. ','',$author)); ?>
                                <?php if ($author == 'Anonymou'): ?>
                                    <span class="meta">by: <a href="<?php echo get_term_link($term); ?>"><span class="author">Anonymous</span></span></a>
                                <?php else: ?>
                                    <span class="meta">by: <a href="<?php echo get_term_link($term); ?>"><span class="author"><?php echo $author; ?></span></span></a>
                                <?php endif;?>
                            <?php endforeach; ?>
                            <?php the_content(); ?>
                            <?php $book_single = get_field('book_single'); ?>
                            <?php if (!empty($book_single['link']['url'])): ?>
                                <a href="<?php echo $book_single['link']['url']; ?>" <?php echo !empty($book_single['link']['target']) ? 'target="'.$book_single['link']['url'].'"':''?> class="btn btn-danger"><?php echo (!empty($book_single['link']['title'])) ? $book_single['link']['title']:'Purchase this book'; ?></a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
