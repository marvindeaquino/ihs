<?php /*get_template_part('templates/content-single', get_post_type()); */?>
<?php get_template_part('templates/page', 'header'); ?>
<?php while (have_posts()) : the_post(); ?>
    <div class="section-single-post section-archive-hormones-single ptb-80">
        <div class="container">
            <div class="content-with-sidebar content-with-sidebar-left">
                <div class="content-main">
                    <div class="post-list-inline">
                        <div class="post-list-inline-details">
                            <?php
                            global $post;
                            $terms = wp_get_post_terms( $post->ID, 'categories_media');
                            ?>
                            <h1><?php the_title()?></h1>
                            <ul class="list-inline meta-list">
                                <li class="list-inline-item"><span class="meta-date"><?php echo get_the_date(); ?></span></li>
                                <?php if (!empty($terms[0]->name) && get_the_date()): ?>
                                    <li class="list-inline-item"><span class="meta-separator">|</span></li>
                                <?php endif;?>
                                <?php if (!empty($terms[0]->name)): ?>

                                    <li class="list-inline-item"><span class="meta-category"><?php echo $terms[0]->name; ?></span></li>
                                <?php endif;?>
                            </ul>
                            <?php if (has_post_thumbnail()): ?>
                                <?php the_post_thumbnail('media-single-thumbnail',array('class'=>'single-post-thumbnail'));?>
                            <?php endif;?>
                            <?php the_content()?>
                        </div>
                    </div>
                </div>
                <?php get_template_part('templates/sidebar', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
            </div>
        </div>
    </div>
<?php endwhile; ?>
