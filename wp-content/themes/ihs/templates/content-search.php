<div class="col-md-6 col-lg-4">
    <div class="card">
        <div class="archive-thumbnail">
            <?php if(!empty(tribe_event_featured_image())):?>
                <?php echo the_post_thumbnail( 'archive-thumbnail'); ?>
            <?php else: ?>
                <a href="http://ihs.lm/event/workshop-healthy-aging-hormone-therapies/">
                    <img width="414" height="217" src="<?php echo get_template_directory_uri()?>/dist/images/events/event-place-holder.jpg">
                </a>
            <?php endif; ?>
        </div>
        <div class="card-body">
            <h3 class="card-title">
                <a href="<?php the_permalink(); ?>" title="Workshop Healthy Aging Hormone Therapies" rel="bookmark"><?php the_title()?></a>
            </h3>
            <div class="card-meta">
                <?php get_template_part('templates/entry-meta'); ?>
            </div>
            <div class="card-excerpt">
                <?php echo get_excerpt(150) ?>
            </div>
        </div>
        <a class="card-footer" href="<?php the_permalink(); ?>"><i class="icon-arrow-pointing-to-right-in-a-circle"></i>Read more</a>
    </div>
</div>