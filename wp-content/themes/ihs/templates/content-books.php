<div class="section-archive ptb-80">
    <div class="container">
        <div class="content-with-sidebar">
            <?php get_template_part('templates/sidebar', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
            <div class="content-main">
                <div class="row">
                    <?php if (!have_posts()) : ?>
                        <div class="col-md-12">
                            <div class="search-result-container">
                                <div class="alert alert-warning">
                                    <?php _e('Sorry, no results were found.', 'sage'); ?>
                                </div>
                                <?php get_template_part('templates/search','box')?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-md-12  col-lg-6">
                            <div class="post-list-inline">
                                <div class="post-list-inline-img">
                                    <?php the_post_thumbnail('books-archive-thumbnail')?>
                                </div>
                                <div class="post-list-inline-details">
                                    <h2><?php the_title(); ?></h2>
                                    <?php $terms = get_terms('book_author'); ?>
                                    <?php foreach ($terms as $term) : ?>
                                        <?php $author = $term->name ?>
                                        <?php $author = str_replace("'s Books","", str_replace('Dr. ','',$author)); ?>
                                        <?php /*if ($author == 'Anonymou'): */?><!--
                                            <span class="meta">by: <a href="<?php /*echo get_term_link($term); */?>"><span class="author">Anonymous</span></span></a>
                                        <?php /*else: */?>
                                            <span class="meta">by: <a href="<?php /*echo get_term_link($term); */?>"><span class="author"><?php /*echo $author; */?></span></span></a>
                                        --><?php /*endif;*/?>
                                        <span class="meta">by: <a href="<?php echo get_term_link($term); ?>"><span class="author"><?php echo $author; ?></span></span></a>
                                    <?php endforeach; ?>
                                    <p><?php echo get_excerpt('70'); ?></p>
                                    <a href="<?php the_permalink()?>" class="text-danger"><i class="icon-arrow-pointing-to-right-in-a-circle"></i>Read More</a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>

                <?php
                $current_post_type = get_post_type();
                $archive = get_field('archive','option');
                $archive_types = $archive['type'];
                ?>
                <?php if ($archive_types):?>
                    <?php foreach ($archive_types as $archive_type):?>
                        <?php if ($archive_type['post_type_slug'] == $current_post_type && !empty($archive_type['link']['url'])): ?>
                            <a href="<?php echo $archive_type['link']['url'];?>" class="btn btn-warning"><?php echo !empty($archive_type['link']['title']) ? $archive_type['link']['title']: 'If you want to publish your books contact us';?></a>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <div class="row">
                    <?php include locate_template('templates/pagination.php');?>
                </div>
            </div>
        </div>
    </div>
</div>
























