<?php $sidebar = get_post_type() != 'post' ? 'sidebar-'.get_post_type() : get_post_format(); ?>
<?php if (is_active_sidebar($sidebar)): ?>
    <div class="content-sidebar">
        <?php dynamic_sidebar($sidebar); ?>
    </div>
<?php endif;?>