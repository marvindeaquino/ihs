<?php

if( have_rows('section_blocks') ):

    while ( have_rows('section_blocks') ) : the_row();

        if( get_row_layout() == 'master_banner' ):

            get_template_part('templates/flex', 'master-banner');

        elseif( get_row_layout() == 'section_welcome' ):

            get_template_part('templates/flex', 'section-welcome');

        elseif( get_row_layout() == 'section_upcoming' ):

            get_template_part('templates/flex', 'section-upcoming');

        elseif( get_row_layout() == 'section_updates' ):

            get_template_part('templates/flex', 'section-updates');

        elseif( get_row_layout() == 'section_board' ):

            get_template_part('templates/flex', 'section-board');


        endif;

    endwhile;

endif;
