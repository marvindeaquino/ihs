<?php $master_banner = get_sub_field('master_banner'); ?>
<?php $master_banner = $master_banner['item'];?>
<?php if ($master_banner):?>
    <div class="section-home-banner">
        <div class="owl-home-carousel owl-carousel">
            <?php foreach ($master_banner as $item):?>
                <div class="owl-home-carousel-item <?php echo $item['with_background_image_extra'] ? 'with-bg-img-extra':''?>" style="background-image: url('<?php echo $item['background_image']['url'];?>'); background-color: <?php echo $item['background_color'];?>;">
                    <div class="container">
                        <div class="content">
                            <?php echo $item['content'];?>
                            <img class="extra-bg" src="<?php echo $item['background_images_extra']['url'];?>" alt="<?php echo $item['background_images_extra']['alt'];?>">
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
