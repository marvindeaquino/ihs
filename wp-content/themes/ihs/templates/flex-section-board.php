<?php $section_board = get_sub_field('section_board'); ?>

<div class="section-board-directors ptb-80">
    <div class="container">
        <div class="content">
            <div class="content-intro"><?php echo $section_board['content']; ?></div>
            <?php if ($board = $section_board['board']):?>
                <div class="card-deck">
                    <?php foreach ($board as $item):?>
                        <div class="card card-board-directors">
                            <?php if ($item['image']): ?>
                                <img src="<?php echo $item['image']['sizes']['board-directors-thumbnail']; ?>" class="card-img-top" alt="<?php echo $item['image']['alt']; ?>">
                             <?php else: ?>
                                <img src="<?php echo get_template_directory_uri()?>/dist/images/board/board-placeholder.jpg" class="card-img-top" alt="">
                            <?php endif; ?>


                            <div class="card-body">
                                <h3 class="card-title"><?php echo $item['title']; ?></h3>
                                <h6 class="card-meta"><?php echo $item['meta']; ?></h6>
                                <h4 class="card-subtitle"><?php echo $item['subtitle']; ?></h4>
                            </div>
                            <?php if (!empty($item['link']['url'])):?>
                                <a href="<?php echo $item['link']['url']; ?>" class="card-footer"><i class="icon-arrow-pointing-to-right-in-a-circle"></i><?php echo !empty($item['link']['title']) ? $item['link']['title']:'Read More' ; ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>