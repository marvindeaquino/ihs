<div class="col-pagination col-12">
    <div class="pagination pagination-default">
        <?php
        $args = array(
            'prev_text' => 'Prev',
            'next_text' => 'Next',
        );
        ?>
        <?php echo paginate_links($args);?>
    </div>
</div>