<?php $section_welcome = get_sub_field('section_welcome'); ?>
<div class="section-welcome ptb-80" style="background-color: <?php $section_welcome['background_color']; ?>">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-l">
                        <?php echo $section_welcome['content']?>
                        <?php if (!empty($section_welcome['link']['url'])): ?>
                            <a href="<?php echo $section_welcome['link']['url']; ?>" class="btn btn-danger"><?php echo !empty($section_welcome['link']['title']) ? $section_welcome['link']['title']:'Find out more';?></a>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="content-r">
                        <img class="img-fluid" src="<?php echo $section_welcome['image']['url']; ?>" alt="<?php echo $section_welcome['image']['alt']; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>