<?php
add_theme_support( 'post-thumbnails' );
add_image_size( 'board-directors-thumbnail', 268, 277, array( 'center', 'center' ));
add_image_size( 'books-archive-thumbnail', 150, 213 );
add_image_size( 'books-single-thumbnail', 260, 360 );
add_image_size( 'evente-archive-thumbnail', 414, 217 );
add_image_size( 'archive-thumbnail', 414, 217 );
add_image_size( 'media-single-thumbnail', 831, 297 );