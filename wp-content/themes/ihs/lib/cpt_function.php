<?php
function cptui_register_my_cpts() {

    /**
     * Post Type: Books.
     */

    $labels = array(
        "name" => __( "Books", "sage" ),
        "singular_name" => __( "Book", "sage" ),
    );

    $args = array(
        "label" => __( "Books", "sage" ),
        "labels" => $labels,
        "description" => "This Book are Hormone Health Topics",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "books", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "books", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_cpts_books() {

    /**
     * Post Type: Books.
     */

    $labels = array(
        "name" => __( "Books", "sage" ),
        "singular_name" => __( "Book", "sage" ),
    );

    $args = array(
        "label" => __( "Books", "sage" ),
        "labels" => $labels,
        "description" => "This Book are Hormone Health Topics",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "books", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "books", $args );
}

add_action( 'init', 'cptui_register_my_cpts_books' );
