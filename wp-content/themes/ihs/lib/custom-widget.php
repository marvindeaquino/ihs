<?php
// Register and load the widget
function wpb_load_widget() {
register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

// Creating the widget
class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'wpb_widget',

// Widget name will appear in UI
__('Featured Media Post', 'wpb_widget_domain'),

// Widget description
array( 'description' => __( 'This widget will show featured post in Media CPT', 'wpb_widget_domain' ), )
);
}

// Creating widget front-end

public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
function featured_post_media () {
    $args = array(
        'post_type' => 'media',
        'tax_query' => array(
            array(
                'taxonomy' => 'categories_media',
                'field'    => 'slug',
                'terms' => 'featured'
            )
        )
    );
    $loop = new WP_Query($args);
    if ( $loop->have_posts() ) :
        echo '<ul>';
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
        <?php endwhile;
        echo '</ul>';
    endif;
    wp_reset_postdata();
}
echo __( featured_post_media(), 'wpb_widget_domain' );
echo $args['after_widget'];
}

// Widget Backend
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
}
} // Class wpb_widget ends here