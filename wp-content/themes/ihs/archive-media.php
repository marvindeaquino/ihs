<?php get_template_part('templates/page', 'header'); ?>
<div class="section-archive section-archive-media ptb-80">
    <div class="container">
        <div class="content-with-sidebar content-with-sidebar-left">
            <div class="content-main">
                <?php while (have_posts()) : the_post(); ?>
                <div class="post-list-inline post-list-inline-media post-list-inline-bordered">
                    <div class="post-list-inline-details">
                        <h2><?php the_title(); ?></h2>
                        <?php
                        global $post;
                        $terms = wp_get_post_terms( $post->ID, 'categories_media');
                        ?>
                        <ul class="list-inline meta-list">
                            <li class="list-inline-item"><span class="meta-date"><?php echo get_the_date(); ?></span></li>
                            <?php if (!empty($terms[0]->name) && get_the_date()): ?>
                                <li class="list-inline-item"><span class="meta-separator">|</span></li>
                            <?php endif;?>
                            <?php if (!empty($terms[0]->name)): ?>

                                <li class="list-inline-item"><span class="meta-category"><?php echo $terms[0]->name; ?></span></li>
                            <?php endif;?>
                        </ul>
                        <p class="excerpt"><?php echo get_excerpt(235)?></p>
                        <a class="link-read-more" href="<?php the_permalink(); ?>"><i class="icon-arrow-pointing-to-right-in-a-circle"></i>Read More</a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php include locate_template('templates/pagination.php');?>
            </div>
            <?php get_template_part('templates/sidebar', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        </div>
    </div>
</div>




























