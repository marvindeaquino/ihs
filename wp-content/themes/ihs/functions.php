<?php define('assets_version_css', '4.4'); ?>
<?php define('assets_version_js', '1.1'); ?>
<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',    // Scripts and stylesheets
    'lib/extras.php',    // Custom functions
    'lib/setup.php',     // Theme setup
    'lib/titles.php',    // Page titles
    'lib/wrapper.php',   // Theme wrapper class
    'lib/navwalker-bs.php', // Navwalker BS
    'lib/customizer.php', // Theme customizer
    'lib/image-thumbnails.php', // Image Thumbnails
    'lib/custom-widget.php', // Image Thumbnails
/*    'lib/cpt_function.php' // Custom Post Type*/
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

//ACF Option
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    acf_add_options_sub_page('Option');
}

//Add Upload Logo
add_theme_support( 'custom-logo' );
function themename_custom_logo_setup() {
    $defaults = array(
        /*'width'       => 283,
        'height'      => 90,*/
        'width'       => 435,
        'height'      => 106,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );


// Remove P Tags Around Images
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

//Remove P
/**
 * Move image inside <p> tag above the <p> tag while preserving any link around image.
 * Can be prevented by adding any attribute or whitespace to <p> tag, e.g. <p class="yolo"> or even <p >
 */
function gc_remove_p_tags_around_images($content)
{
    $contentWithFixedPTags =  preg_replace_callback('/<p>((?:.(?!p>))*?)(<a[^>]*>)?\s*(<img[^>]+>)(<\/a>)?(.*?)<\/p>/is', function($matches)
    {
        /*
        Groups 	Regex 			Description
            <p>			starting <p> tag
        1	((?:.(?!p>))*?)		match 0 or more of anything not followed by p>
                .(?!p>) 		anything that's not followed by p>
              ?: 			non-capturing group.
                    *?		match the ". modified by p> condition" expression non-greedily
        2	(<a[^>]*>)?		starting <a> tag (optional)
            \s*			white space (optional)
        3	(<img[^>]+>)		<img> tag
            \s*			white space (optional)
        4	(<\/a>)? 		ending </a> tag (optional)
        5	(.*?)<\/p>		everything up to the final </p>
            i modifier 		case insensitive
            s modifier		allows . to match multiple lines (important for 1st and 5th group)
        */

        // image and (optional) link: <a ...><img ...></a>
        $image = $matches[2] . $matches[3] . $matches[4];
        // content before and after image. wrap in <p> unless it's empty
        $content = trim( $matches[1] . $matches[5] );
        if ($content) {
            $content = '<p>'. $content .'</p>';
        }
        return $image . $content;
    }, $content);

    // On large strings, this regular expression fails to execute, returning NULL
    return is_null($contentWithFixedPTags) ? $content : $contentWithFixedPTags;
}

//Limit the number of event desplay
function num_posts_archive_project_ie($query) {
    if (!is_admin() && $query->is_archive('events') && $query->is_main_query()) {
        $query->set('posts_per_page', 6);
    }
    return $query;
}

/* Function for geting the category name from event calendar plugin */
/* Please use "tribe_get_text_categories ()" to use the function */
if ( class_exists('Tribe__Events__Main') ){

    /* get event category names in text format */
    function tribe_get_text_categories ( $event_id = null ) {

        if ( is_null( $event_id ) ) {
            $event_id = get_the_ID();
        }

        $event_cats = '';

        $term_list = wp_get_post_terms( $event_id, Tribe__Events__Main::TAXONOMY );

        foreach( $term_list as $term_single ) {
            $event_cats .= $term_single->name . ', ';
        }

        return rtrim($event_cats, ', ');

    }

}
/* Restore to Plain Catname */
function replace_dashes_to_space($string) {
    $string = ucwords(str_replace("-", " ", $string));
    return $string;
}

function replace_dashes($string) {
    $string = strtolower(str_replace(" ", "-", $string));
    return $string;
}


add_filter('tribe_events_template_data_array', 'tec_forum_json', 10, 3);
function tec_forum_json( $json, $event, $additional ){

    if(isset($json['eventId'])){

        preg_match('/[^-]*/', $json['eventId'], $match);
        $id = $match[0]? $match[0] : null;
        $category_name = tribe_get_text_categories($id);
        $event_img = get_field('event_thumb', $id);
        $post_img = $event_img['url'];
        $json['category_name'] = $category_name;
        $json['img_thumbnail'] = $post_img;
        $json['excerpt'] = substr($json['excerpt'], 0, 80) . '...';
        $json['category_name_class'] = replace_dashes($category_name);
        $json['i18n']['find_out_more'] = "View Details";
    }
    return $json;
}

add_action('tribe_events_pre_get_posts', 'override_tribe_events_pre_get_posts');
function override_tribe_events_pre_get_posts($query)
{
    $get_month = isset($_GET['month'])?$_GET['month']:'';
    $get_country = isset($_GET['country'])?(strpos($_GET['country'],'Select')===false?$_GET['country']:''):'';
    $meta_query = (array) $query->get( 'meta_query' );

    if($get_country != '')
    {
        $meta_query[] = array(
            'key' => '_VenueCountry',
            'value' => $get_country,
            'compare' => '=',
        );
    }

    if($get_month != '')
        $query->set( 'tribe_event_month_filter', $get_month );

    $query->set( 'meta_query', $meta_query );
    $query->set( 'start_date', '' );
    if(!is_front_page()) {
        $query->set( 'posts_per_page', -1 );
    }
    $query->set( 'order', 'DESC' );
}

add_filter('tribe_events_query_posts_joins', 'custom_tribe_events_query_posts_joins');
function custom_tribe_events_query_posts_joins($joins)
{
    global $wpdb;
    $get_country = isset($_GET['country'])?(strpos($_GET['country'],'Select')===false?$_GET['country']:''):'';

    if($get_country != '')
    {
        $joins['venue'] = " LEFT JOIN {$wpdb->postmeta} as tribe_event_venue ON ( {$wpdb->posts}.ID = tribe_event_venue.post_id AND tribe_event_venue.meta_key = '_EventVenueID' ) ";

        $joins['venue_country'] = " LEFT JOIN {$wpdb->postmeta} as tribe_event_venue_country ON ( tribe_event_venue.meta_value = tribe_event_venue_country.post_id AND tribe_event_venue_country.meta_key = '_VenueCountry' ) ";
    }

    return $joins;
}

add_filter( 'meta_query_find_compatible_table_alias', 'custom_meta_query_find_compatible_table_alias', 11, 2 );
function custom_meta_query_find_compatible_table_alias( $alias, $meta_query )
{
    if ( '_VenueCountry' == $meta_query['key'] )
        return 'tribe_event_venue_country';

    return $alias;
}

add_filter( 'posts_where', 'custom_posts_where', 11, 2 );
function custom_posts_where($where_sql, $query)
{
    global $wpdb;

    // for tribe event listing page only
    if (
        (
            $query->tribe_is_event
            || $query->tribe_is_event_category
        )
        && empty( $query->query_vars['name'] )
        && empty( $query->query_vars['p'] )
    ) {
        $tribe_event_month_filter = $query->get('tribe_event_month_filter');
        if($tribe_event_month_filter)
            $where_sql .= " AND ( DATE_FORMAT(ihs_postmeta.meta_value, '%m') = '".$tribe_event_month_filter."')";
    }
    return $where_sql;
}


/*
Sample...  Lorem ipsum habitant morbi (26 characters total)

Returns first three words which is exactly 21 characters including spaces
Example..  echo get_excerpt(21);
Result...  Lorem ipsum habitant

Returns same as above, not enough characters in limit to return last word
Example..  echo get_excerpt(24);
Result...  Lorem ipsum habitant

Returns all 26 chars of our content, 30 char limit given, only 26 characters needed.
Example..  echo get_excerpt(30);
Result...  Lorem ipsum habitant morbi
*/
function get_excerpt($limit, $source = null){

    $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    return $excerpt;
}


function custom_post_type_resources_query( $query ){
    if(! is_admin() && $query->is_post_type_archive( 'hormones' ) && $query->is_main_query() ){
        $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'custom_post_type_resources_query' );