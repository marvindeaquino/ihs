<?php
while (have_posts()) : the_post();
    if( !is_front_page() ) {
        get_template_part('templates/page', 'header');
    }

    get_template_part('templates/flex', '');

    if( '' !== get_post()->post_content ) {

        echo '<div class="section-default-page ptb-80"><div class="container">';

        get_template_part('templates/content', 'page');

        echo '</div></div>';
    }
endwhile;