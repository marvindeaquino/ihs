<?php get_template_part('templates/page', 'header'); ?>
<div class="section ptb-80">
    <div class="container">
        <div class="search-result-container">
            <div class="alert alert-warning">
                <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
            </div>
            <?php get_template_part('templates/search','box')?>
        </div>

    </div>
</div>



