<?php
/**
 * Single Event Meta (Venue) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/venue.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 */

if ( ! tribe_get_venue_id() ) {
	return;
}

$phone   = tribe_get_phone();
$website = tribe_get_venue_website_link();

?>

		<?php do_action( 'tribe_events_single_meta_venue_section_start' ) ?>

<li class="tribe-events-details venue-list-item"><span class="venue-label">Venue Name: </span><span class="tribe-venue"><?php echo tribe_get_venue(); ?></span></li>

		<?php if ( tribe_address_exists() ) : ?>
			<li class="tribe-events-details venue-list-item">
                <span class="tribe-venue-location-label">Location: </span>
				<address class="tribe-events-address">
                    <?php if ( tribe_show_google_map_link() ) : ?>
                        <a href="<?php echo tribe_get_map_link(); ?>">
                    <?php endif;?>
                        <?php echo tribe_get_full_address(); ?>
                    <?php if ( tribe_show_google_map_link() ) : ?>
                        </a>
					<?php endif; ?>
				</address>
			</li>
		<?php endif; ?>

		<?php if ( ! empty( $phone ) ): ?>
            <li class="tribe-events-details venue-list-item">
                <span class="tribe-venue-tel-label"> <?php esc_html_e( 'Phone:', 'the-events-calendar' ) ?> </span>
                <span class="tribe-venue-tel"> <?php echo $phone ?> </span>
            </li>
		<?php endif ?>

		<?php if ( ! empty( $website ) ): ?>
            <li class="tribe-events-details venue-list-item">
                <span class="tribe-venue-url-label"> <?php esc_html_e( 'Website:', 'the-events-calendar' ) ?> </span>
                <span class="tribe-venue-url"> <?php echo $website ?> </span>
            </li>
		<?php endif ?>

		<?php do_action( 'tribe_events_single_meta_venue_section_end' ) ?>


