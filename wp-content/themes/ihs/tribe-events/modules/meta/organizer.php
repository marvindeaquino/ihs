<?php
/**
 * Single Event Meta (Organizer) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/organizer.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 */

$organizer_ids = tribe_get_organizer_ids();
$multiple = count( $organizer_ids ) > 1;

$phone = tribe_get_organizer_phone();
$email = tribe_get_organizer_email();
$website = tribe_get_organizer_website_link();
?>

		<?php
		do_action( 'tribe_events_single_meta_organizer_section_start' );

		foreach ( $organizer_ids as $organizer ) {
			if ( ! $organizer ) {
				continue;
			}

			?>
			<li class="tribe-events-details organizer-list-item" style="display:none;"><?php // This element is just to make sure we have a valid HTML ?></li>
			<li class="tribe-events-details organizer-list-item tribe-organizer">
                <span>Organizer: </span>
                <span><?php echo tribe_get_organizer_link( $organizer ) ?></span>

			</li>
			<?php
		}

		if ( ! $multiple ) { // only show organizer details if there is one
			if ( ! empty( $phone ) ) {
				?>
                <li class="tribe-events-details organizer-list-item">
                    <span class="tribe-organizer-tel-label">
                        <?php esc_html_e( 'Phone:', 'the-events-calendar' ) ?>
                    </span>
                    <span class="tribe-events-details organizer-list-item tribe-organizer-tel">
                        <?php echo esc_html( $phone ); ?>
                    </span>
                </li>

				<?php
			}//end if

			if ( ! empty( $email ) ) {
				?>
                <li class="tribe-events-details organizer-list-item">
                    <span class="tribe-organizer-email-label">
                        <?php esc_html_e( 'Email:', 'the-events-calendar' ) ?>
                    </span>
                    <span class="tribe-organizer-email">
                        <?php echo esc_html( $email ); ?>
                    </span>
                </li>
				<?php
			}//end if

			if ( ! empty( $website ) ) {
				?>
                <li class="tribe-events-details organizer-list-item">
                    <li class="tribe-organizer-url-label">
                        <?php esc_html_e( 'Website:', 'the-events-calendar' ) ?>
                    </li>
                    <li class="tribe-organizer-url">
                        <?php echo $website; ?>
                    </li>
                </li>

				<?php
			}//end if
		}//end if

		do_action( 'tribe_events_single_meta_organizer_section_end' );
		?>

