<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>
<div class="section-tribe-events-single ptb-80">
    <div class="container">
        <?php tribe_get_template_part( 'modules/meta/map' ); ?>
        <div class="tribe-events-single-with-sidebar">
            <div id="tribe-events-content" class="tribe-events-single tribe-events-single-main">
                <!-- Notices -->
                <?php tribe_the_notices() ?>

                <?php the_title( '<h2 class="tribe-events-single-event-title">', '</h1>' ); ?>

                <?php while ( have_posts() ) :  the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <!-- Event featured image, but exclude link -->
                        <?php /*echo tribe_event_featured_image( $event_id, 'full', false ); */?>

                        <!-- Event content -->
                        <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
                        <div class="tribe-events-single-event-description tribe-events-content">
                            <?php the_content(); ?>
                        </div>
                        <!-- .tribe-events-single-event-description -->
                        <?php /*do_action( 'tribe_events_single_event_after_the_content' ) */?>
                    </div> <!-- #post-x -->
                    <?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
                <?php endwhile; ?>
            </div><!-- #tribe-events-content -->
            <div class="tribe-events-single-sidebar">
                <?php while ( have_posts() ) :  the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <h2>Event Details</h2>
                        <ul>
                        <!-- Event meta -->
                        <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                        <?php tribe_get_template_part( 'modules/meta' ); ?>
                        <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
                        </ul>
                    </div> <!-- #post-x -->
                    <?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
