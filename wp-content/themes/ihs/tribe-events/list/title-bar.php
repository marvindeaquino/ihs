<?php
/**
 * List View Title Template
 * The title template for the list view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<div class="tribe-events-title-bar">

	<!-- List Title -->
	<?php do_action( 'tribe_events_before_the_title' ); ?>
	<h1 class="tribe-events-page-title"><?php echo tribe_get_events_title() ?></h1>
	<?php do_action( 'tribe_events_after_the_title' ); ?>
    <div class="search-filter">
        <?php
            $hold1 = isset($_GET['month']) ? $_GET['month'] : '';
            $hold2 = isset($_GET['country']) ? $_GET['country'] : '';
            global $wp;
            $current_url = home_url( $wp->request ).'/';
            $months = [
                    "01" => "January",
                    "02" => "February",
                    "03" => "March",
                    "04" => "April",
                    "05" => "May",
                    "06" => "June",
                    "07" => "July",
                    "08" => "August",
                    "09" => "September",
                    "10" => "October",
                    "11" => "November",
                    "12" => "December",
            ];
        ?>
        <form action="<?php echo $current_url;?>" method="GET" id="newslist">
            <label for="exampleFormControlSelect1">Filter By:</label>
            <div class="form-group">
                <select class="custom-select" name="month" id="month" onchange="submit();">
                    <option value="">Month</option>
                    <?php
                    foreach ($months as $key=> $month) :
                        echo '<option value="'.$key.'"';
                        echo ($hold1 == ''.$key.'') ? ' selected="selected"' : '';
                        echo '>'.$month.'</option>';
                    endforeach;
                    ?>
                </select>
                <select class="custom-select" name="country" id="country" onchange="submit();">
                    <?php
                    $countries = Tribe__View_Helpers::constructCountries( $post->ID );
                    foreach ($countries as $country) :
                        echo '<option value="'.$country.'"';
                        echo ($hold2 == ''.$country.'') ? ' selected="selected"' : '';
                        echo '>'.str_replace('Select a Country:','Country', $country).'</option>';
                    endforeach;
                    ?>
                </select>
                <a href="/events" class="btn btn-danger">Clear</a>
            </div>

        </form>
    </div>
</div>
