<?php
/**
 * List View Loop
 * This file sets up the structure for the list loop
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/loop.php
 *
 * @version 4.4
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}
global $wp_query;
?>

<div class="row row-tribe-events-loop">
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-md-6 col-lg-4">
                <?php do_action( 'tribe_events_inside_before_loop' ); ?>
                <!-- Event  -->
                <?php
                $post_parent = '';
                if ( $post->post_parent ) {
                    $post_parent = ' data-parent-post-id="' . absint( $post->post_parent ) . '"';
                }
                ?>
                <div class="card" <?php echo $post_parent; ?>>
                    <?php
                    $event_type = tribe( 'tec.featured_events' )->is_featured( $post->ID ) ? 'featured' : 'event';

                    /**
                     * Filters the event type used when selecting a template to render
                     *
                     * @param $event_type
                     */
                    $event_type = apply_filters( 'tribe_events_list_view_event_type', $event_type );

                    tribe_get_template_part( 'list/single', $event_type );
                    ?>
                </div>


                <?php do_action( 'tribe_events_inside_after_loop' ); ?>
            </div>
        <?php endwhile; ?>
        <div class="col-pagination col-sm-12">
            <div class="pagination pagination-default">
                <?php
                $big = 999999999;
                $args = array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $wp_query->max_num_pages,
                    'prev_text' => '«',
                    'next_text' => '»',
                );
                ?>
                <?php echo paginate_links($args);?>
            </div>
        </div>

    <?php endif; ?>
</div>
<!-- .row-tribe-events-loop -->