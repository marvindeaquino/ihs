<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.6.19
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// The address string via tribe_get_venue_details will often be populated even when there's
// no address, so let's get the address string on its own for a couple of checks below.
$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>
<!-- Event Image -->
    <?php if(!empty(tribe_event_featured_image())):?>
        <?php echo tribe_event_featured_image( null, 'evente-archive-thumbnail' ); ?>
    <?php else: ?>
        <div class="tribe-events-event-image">
            <a href="http://ihs.lm/event/workshop-healthy-aging-hormone-therapies/">
                <img width="414" height="217" src="<?php echo get_template_directory_uri()?>/dist/images/events/event-place-holder.jpg" class="attachment-evente-archive-thumbnail size-evente-archive-thumbnail wp-post-image" alt="">
            </a>
        </div>
    <?php endif; ?>


<div class="card-body">
    <!-- Event Title -->
    <?php do_action( 'tribe_events_before_the_event_title' ) ?>
    <h3 class="card-title tribe-events-list-event-title">
        <a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
            <?php the_title() ?>
        </a>
    </h3>
    <?php do_action( 'tribe_events_after_the_event_title' ) ?>

    <!-- Event Meta -->
    <?php do_action( 'tribe_events_before_the_meta' ) ?>
    <div class="card-meta tribe-events-event-meta">
        <div class="author <?php echo esc_attr( $has_venue_address ); ?>">
            <?php if ( $venue_details ) : ?>
                <!-- Venue Display Info -->
                <div class="tribe-events-venue-details">
                    <span class="meta-location"><i class="icon-pin"></i> <?php echo tribe_get_city(); ?>, <?php echo tribe_get_country(); ?></span>
                </div> <!-- .tribe-events-venue-details -->
            <?php endif; ?>
            <!-- Schedule & Recurrence Details -->
            <div class="tribe-event-schedule-details">
                <span class="schedule-label">Schedule</span>
                <?php echo tribe_events_event_schedule_details() ?>

            </div>



        </div>
    </div><!-- .tribe-events-event-meta -->
</div>
<?php do_action( 'tribe_events_after_the_meta' ) ?>



<!-- Event Content -->
<?php do_action( 'tribe_events_before_the_content' ); ?>

    <a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="card-footer tribe-events-read-more" rel="bookmark">

        <i class="icon-arrow-pointing-to-right-in-a-circle"></i>
        <?php esc_html_e( 'Read more', 'the-events-calendar' ) ?>
    </a>


<?php
do_action( 'tribe_events_after_the_content' );