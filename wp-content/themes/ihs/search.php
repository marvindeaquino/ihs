<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
    <div class="section-search ptb-80">
        <div class="container">
            <div class="search-result-container">
                <div class="alert alert-warning">
                    <?php _e('Sorry, no results were found.', 'sage'); ?>
                </div>
                <?php get_template_part('templates/search','box')?>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="section-archive section-archive-hormones">
        <?php include locate_template('templates/archive-content.php');?>
        <div class="archive-hormones-content-loop ptb-80">
            <div class="container">
                <div class="card-deck">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            </div>
                            <div class="card-body">
                                <p class="card-text">
                                    <?php echo get_excerpt(91); ?>
                                </p>
                                <a class="card-link" href="<?php the_permalink(); ?>"><i class="icon-arrow-pointing-to-right-in-a-circle"></i>Read More</a>
                            </div>

                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="row">
                    <?php include locate_template('templates/pagination.php');?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>















































